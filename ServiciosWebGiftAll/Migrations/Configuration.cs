namespace ServiciosWebGiftAll.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ServiciosWebGiftAll.Models;


    internal sealed class Configuration : DbMigrationsConfiguration<ServiciosWebGiftAll.Models.GiftAllDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ServiciosWebGiftAll.Models.GiftAllDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Perfiles.AddOrUpdate(p => p.Nombre, new Cl_Perfil() { Id = 1, Nombre = "administrador" },new  Cl_Perfil{ Id=2, Nombre="normal" });
            context.Usuarios.AddOrUpdate(u => u.Correo, new Cl_Usuario() { Id = 1, Correo = "admin@admin.cl", Password = "qwerty", PerfilId = 1 });
        }
    }
}

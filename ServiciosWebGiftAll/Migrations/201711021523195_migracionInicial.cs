namespace ServiciosWebGiftAll.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migracionInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cl_Usuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Correo = c.String(),
                        Password = c.String(),
                        PerfilId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cl_Perfil", t => t.PerfilId, cascadeDelete: true)
                .Index(t => t.PerfilId);
            
            CreateTable(
                "dbo.Cl_Perfil",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cl_Usuario", "PerfilId", "dbo.Cl_Perfil");
            DropIndex("dbo.Cl_Usuario", new[] { "PerfilId" });
            DropTable("dbo.Cl_Perfil");
            DropTable("dbo.Cl_Usuario");
        }
    }
}

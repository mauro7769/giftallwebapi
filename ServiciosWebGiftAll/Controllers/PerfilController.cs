﻿using ServiciosWebGiftAll.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiciosWebGiftAll.Controllers
{
    public class PerfilController : ApiController
    {
        private GiftAllDbContext context;

        public PerfilController()
        {
            //instanciamos el contexto que nos permite
            //acceso a los datos
            context = new GiftAllDbContext();
        }

        public IEnumerable<Object> get()
        {
            //que me retorne todos los perfiles
            return context.Perfiles.ToList();
        }
    }
}

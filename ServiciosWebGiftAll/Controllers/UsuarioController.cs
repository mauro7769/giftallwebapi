﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ServiciosWebGiftAll.Models;

namespace ServiciosWebGiftAll.Controllers
{
    public class UsuarioController : ApiController
    {
        private GiftAllDbContext context;

        public UsuarioController()
        {
            this.context = new GiftAllDbContext();
        }

        public IEnumerable<Object> get()
        {
            return context.Usuarios.Include("Perfiles").Select(u => new
            {
                Id = u.Id,
                Correo = u.Correo,
                Password = u.Password,
                Perfil = new {
                    Id = u.Perfil.Id,
                    Nombre = u.Perfil.Nombre
                }
            });
        }

        //api/Usuario/{id}
        public IHttpActionResult get(int id)
        {
            Cl_Usuario usuario = context.Usuarios.Find(id);

            if (usuario == null)//404 notfound
            {
                return NotFound();
            }


            return Ok(usuario);//retornamos codigo 200 junto con el usuario buscado
        }

        //api/Usuario/{correo,password}
        public IHttpActionResult validacion(string correo, string password)
        {
            Cl_Usuario usuario = context.Usuarios.Find(correo);

            if (usuario == null)//404 notfound
            {
                return NotFound();
            }
            else {
                if (usuario.Correo.Equals(correo) && usuario.Password.Equals(password))
                {
                    return Ok(usuario);//retornamos codigo 200 junto con el usuario buscado
                }
                return NotFound();
               
            }
        }


            


        //api/Usuario
        public IHttpActionResult post(Cl_Usuario usuario)
        {

            context.Usuarios.Add(usuario);
            int filasAfectadas = context.SaveChanges();

            if (filasAfectadas == 0)
            {
                return InternalServerError();//500
            }

            return Ok(new { mensaje = "Agregado correctamente" });

        }


        //api/Usuario/{id}
        public IHttpActionResult delete(int id)
        {
            //buscamos el usuario a eliminar
            Cl_Usuario usuario = context.Usuarios.Find(id);

            if (usuario == null) return NotFound();//404

            context.Usuarios.Remove(usuario);

            if (context.SaveChanges() > 0)
            {
                //retornamos codigo 200
                return Ok(new { Mensaje = "Eliminado correctamente" });
            }

            return InternalServerError();//500

        }



        public IHttpActionResult put(Cl_Usuario usuario)
        {
            context.Entry(usuario).State = System.Data.Entity.EntityState.Modified;

            if (context.SaveChanges() > 0)
            {
                return Ok(new { Mensaje = "Modificado correctamente" });
            }

            return InternalServerError();



        }
    }
}

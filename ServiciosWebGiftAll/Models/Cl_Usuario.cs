﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class Cl_Usuario
    {
        public int Id { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public int PerfilId { get; set; }
        public Cl_Perfil Perfil { get; set; }
        
    }
}
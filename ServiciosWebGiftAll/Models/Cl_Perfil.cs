﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWebGiftAll.Models
{
    public class Cl_Perfil
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public List<Cl_Usuario> Usuarios { get; set; }
    }
}
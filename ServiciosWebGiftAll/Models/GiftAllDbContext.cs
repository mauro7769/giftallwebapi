﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace ServiciosWebGiftAll.Models
{
    public class GiftAllDbContext:DbContext
    {
        public DbSet<Cl_Usuario> Usuarios { get; set; }
        public DbSet<Cl_Perfil> Perfiles { get; set; }
    }
}